winfiles
========

A bunch of settings to help make Windows more bearable.

> Note: The instructions and scripts contained here assume that this 'winfiles' directory is found within your USERPROFILE directory, typically 'C:/Users/username/winfiles'.

Before you install
------------------

Install the following:

 - [Cmder](http://gooseberrycreative.com/cmder/)
 - [cppcheck](http://cppcheck.sourceforge.net/) - Add it to your path.
 - [CTags](http://ctags.sourceforge.net/) - Also add this to your path.
 - [Sublime Text 3](http://www.sublimetext.com/3)
 - [Rapid Environment Editor](http://www.rapidee.com/en/about)

And do these:

 - [Install Sublime Text Package Control](https://packagecontrol.io/installation#st3)
 - [Install Source Code Pro font](http://adobe-fonts.github.io/source-code-pro/)

Install Sublime Text settings
-----------------------------

Until there is a script... Quit Sublime Text and in a terminal with Administrator privileges...

```
# quit Sublime Text
cd "%appdata%\Sublime Text 3\Packages\"
rm -r User
mklink /D User "%userprofile%\winfiles\sublime\User"
```
